<?php 
/*
Plugin Name: Rebel City Tour Booking
Description: Rebel City Tour Booking
Plugin URI: https://rebelcitytour.com/
Author URI: https://rebelcitytour.com/
Author: REBEL CITY TOUR
License: Public Domain
Version: 1.1
*/

require_once 'vendor/autoload.php';
require('classes/TourBooking.php');

date_default_timezone_set('Europe/Dublin');

/* Map booking form template and its URL */
function rebel_city_tour_booking_template()
{   
    $temps = [];
    $temps['rebel-city-tour-booking.php'] = 'Rebel City Tour Booking'; 
    $temps['calendar-events.php'] =  'Calendar Events';
    return $temps;
}

/* Register template on plugin activation */
function register_rebel_city_tour_booking_template($page_templates, $theme, $post)
{
    $templates = rebel_city_tour_booking_template();
    foreach($templates as $key=>$val){
        $page_templates[$key] = $val;
    }
    return $page_templates;
}
add_filter('theme_page_templates','register_rebel_city_tour_booking_template',10,3);

/* Select booking form template and create page */
function select_rebel_city_tour_booking_template($template)
{
    global $post, $wp_query, $wpdb;
    if(isset($post->ID))
    {
        $page_temp_slug = get_page_template_slug($post->ID);
        
        $templates = rebel_city_tour_booking_template();
        
        if(isset($templates[$page_temp_slug])){
            $template = plugin_dir_path(__FILE__).'templates/'.$page_temp_slug;   
        }
    }
    return $template;
}

add_filter('template_include','select_rebel_city_tour_booking_template',99);

function deactivate_plugin() 
{
    global $wpdb;
    $table = $wpdb->prefix.'posts';
    $query = new WP_Query(
        array(
            'post_type'              => 'page',
            'title'                  => 'Rebel City Tour Booking',
            'post_status'            => 'all',
            'posts_per_page'         => 1,
            'no_found_rows'          => true,
            'ignore_sticky_posts'    => true,
            'update_post_term_cache' => false,
            'update_post_meta_cache' => false,
            'orderby'                => 'post_date ID',
            'order'                  => 'ASC',
        )
    );
     
    if ( ! empty( $query->post ) ) 
    {
        $page_id = $query->post->ID;
        wp_delete_post($page_id,true);
        wp_reset_postdata ();
        $wpdb->delete( $table, array( 'id' => $page_id ) );
    } 
    else 
    {
        $page_id = null;
    }
}
register_deactivation_hook( __FILE__, 'deactivate_plugin' );

function rebel_city_tour_booking_shortcode( $atts ) {
    global $post, $wp_query, $wpdb, $woocommerce;
    ob_start();  
    include(plugin_dir_path( __FILE__ ) . 'templates/rebel-city-tour-booking.php');
    $ret = ob_get_contents();  
    ob_end_clean();  
    // print_r($ret); exit;
    return $ret;    
}
add_shortcode( 'rebel_city_tour_booking_shortcode', 'rebel_city_tour_booking_shortcode');


function add_rebel_city_tour_booking_page() 
{
    // Create post object
    $rebel_city_tour_booking = array(
      'post_title'    => wp_strip_all_tags( 'Rebel City Tour Booking' ),
      'post_content'  => 'NA',
      'post_status'   => 'publish',
      'post_author'   => 1,
      'post_type'     => 'page',
      'page_template'  => 'rebel-city-tour-booking.php'
    );
    wp_insert_post( $rebel_city_tour_booking );
}

register_activation_hook(__FILE__, 'add_rebel_city_tour_booking_page');

/* Create payment intent and customer */  

function create_payment_intent() 
{      
    header('Content-Type: application/json'); 
    $postData = json_decode(file_get_contents("php://input"), true)['requestData'];
    
    $experience_ids = isset($postData['experience_ids'])&&!empty($postData['experience_ids'])?$postData['experience_ids']:'';
    $ticket_qty = isset($postData['ticket_qty'])&&!empty($postData['ticket_qty'])?$postData['ticket_qty']:'';
    
    $response = array();
    if(empty($experience_ids))
    {
        $response['status'] = 400;
        $response['data'] = '';
        $response['message'] = 'Please choose at least one experience';
    }
    else
    {   
        $tour_booking = new TourBooking();
        $response = $tour_booking->create_tour_booking_payment_intent_call($experience_ids,$ticket_qty);
       
    }
    $response = json_encode($response);
    echo $response;
    exit;
}

add_action( 'wp_ajax_nopriv_create_payment_intent', 'create_payment_intent' );
add_action( 'wp_ajax_create_payment_intent', 'create_payment_intent' );

add_action( 'admin_post_nopriv_create_payment_intent', 'create_payment_intent' );
add_action( 'admin_post_create_payment_intent', 'create_payment_intent' );

/* Rebel city tour form handling */  
function rebel_city_tour_booking() 
{   
    header('Content-Type: application/json'); 
    $postData = json_decode(file_get_contents("php://input"), true)['requestDataTourBooking'];
    
    $tour_data['experience_id'] = isset($postData['experience_id'])&&!empty($postData['experience_id'])?$postData['experience_id']:'';
    $tour_data['customer_name'] = isset($postData['customer_name'])&&!empty($postData['customer_name'])?$postData['customer_name']:'';
    $tour_data['customer_email'] = isset($postData['customer_email'])&&!empty($postData['customer_email'])?$postData['customer_email']:'';
    $tour_data['customer_phone'] = isset($postData['customer_phone'])&&!empty($postData['customer_phone'])?$postData['customer_phone']:'';
    $tour_data['experience_date'] = isset($postData['experience_date'])&&!empty($postData['experience_date'])?$postData['experience_date']:'';
    $tour_data['experience_time'] = isset($postData['experience_time'])&&!empty($postData['experience_time'])?$postData['experience_time']:'';
    $tour_data['appointment_reminder'] = isset($postData['appointment_reminder'])&&!empty($postData['appointment_reminder'])?$postData['appointment_reminder']:0;
    $tour_data['newsletter_subscription'] = isset($postData['newsletter_subscription'])&&!empty($postData['newsletter_subscription'])?$postData['newsletter_subscription']:0;
    $tour_data['payment_intent_id'] = isset($postData['payment_intent_id'])&&!empty($postData['payment_intent_id'])?$postData['payment_intent_id']:'';
    $tour_data['tour_note'] = isset($postData['tour_note'])&&!empty($postData['tour_note'])?$postData['tour_note']:'None';
    $tour_data['ticket_qty'] = isset($postData['ticket_qty'])&&!empty($postData['ticket_qty'])?$postData['ticket_qty']:1;
   
    
    /* Calling TourBooking class function using its object */
  
    $response = array();
    if(empty($tour_data['payment_intent_id']))
    {
        $response['status'] = 400;
        $response['data'] = '';
        $response['message'] = 'Payment intent id is required';
    }
    else
    {   
        $tour_booking = new TourBooking();
        $response = $tour_booking->create_tour_booking($tour_data);
       
    }
    $response = json_encode($response);
    echo $response;
    exit;
}

add_action( 'wp_ajax_nopriv_rebel_city_tour_booking', 'rebel_city_tour_booking' );
add_action( 'wp_ajax_rebel_city_tour_booking', 'rebel_city_tour_booking' );

add_action( 'admin_post_nopriv_rebel_city_tour_booking', 'rebel_city_tour_booking' );
add_action( 'admin_post_rebel_city_tour_booking', 'rebel_city_tour_booking' );

/* Update payment record */  
function update_payment() 
{      
    header('Content-Type: application/json'); 
    $postData = json_decode(file_get_contents("php://input"), true)['requestPaymentData'];
    $payment_intent = isset($postData['payment_intent'])&&!empty($postData['payment_intent'])?$postData['payment_intent']:'';
    
    $response = array();
    if(empty($payment_intent))
    {
        $response['status'] = 400;
        $response['data'] = '';
        $response['message'] = 'Invalid payment request';
    }
    else
    {   
        $tour_booking = new TourBooking();
        $response = $tour_booking->update_tour_booking_payment($postData);
       
    }
    $response = json_encode($response);
    echo $response;
    exit;
}

add_action( 'wp_ajax_nopriv_update_payment', 'update_payment' );
add_action( 'wp_ajax_update_payment', 'update_payment' );

add_action( 'admin_post_nopriv_update_payment', 'update_payment' );
add_action( 'admin_post_update_payment', 'update_payment' );


/* Create tour booking table if not exists */
function create_tour_booking()
{
    global $wpdb;
    $table_name = $wpdb->prefix . "tour_bookings";
    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE IF NOT EXISTS $table_name (  `id` int NOT NULL AUTO_INCREMENT,
    `experience_id` text DEFAULT NULL,
    `experience_detail` text DEFAULT NULL,
    `experience_duration` text,
    `experience_cost` float DEFAULT NULL,
    `customer_name` text DEFAULT NULL,
    `customer_email` text DEFAULT NULL,
    `customer_phone` text DEFAULT NULL,
    `tour_date` text DEFAULT NULL,
    `tour_time` text DEFAULT NULL,
    `appointment_reminder` tinyint(1) NOT NULL DEFAULT '0',
    `newsletter_subscription` tinyint(1) NOT NULL DEFAULT '0',
    `tour_note` text DEFAULT NULL,
    `ticket_qty` int(11) DEFAULT 1,
    `payment_status` tinyint(1) NOT NULL DEFAULT '0',
    `booking_status` tinyint(1) NOT NULL DEFAULT '0',
    `stripe_customer_id` text,
    `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` datetime DEFAULT NULL,
    PRIMARY KEY (`id`))";
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);
}
register_activation_hook(__FILE__, 'create_tour_booking');

/* Create tour booking table if not exists */
function create_tour_payment()
{
    global $wpdb;
    $table_name = $wpdb->prefix . "tour_payments";
    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE IF NOT EXISTS $table_name (  `id` int NOT NULL AUTO_INCREMENT,
    `booking_id` int DEFAULT NULL,
    `txn_id` text DEFAULT NULL,
    `paid_amount` float DEFAULT NULL,
    `paid_amount_currency` varchar(20) NOT NULL DEFAULT 'EUR',
    `payment_status` text,
    `customer_name` text,
    `customer_email` text,
    `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` datetime DEFAULT NULL,
    PRIMARY KEY (`id`))";
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);
}
register_activation_hook(__FILE__, 'create_tour_payment');
