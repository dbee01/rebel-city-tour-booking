<?php
/**
 * template name: Rebel City Tour Booking
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Rebel City Tour Booking
 */

/* Tour booking form template */
    require('header.php');

?>

<body>
    <div>
        <div id="loader" class="loader">
        </div>
        <!-- <h1 class="d-flex justify-content-center">Rebel City Tour Booking</h1> -->
        <div class="ml-2 mr-2 col-md-12 tour-booking d-flex justify-content-center align-items-center d-none" id="payment-confirmation">
            <div>
                <div class="text-center payment-status d-none" id="payment-completed">
                    <div class="mb-4 text-center">
                        <svg xmlns="http://www.w3.org/2000/svg" class="text-success" width="75" height="75"
                            fill="currentColor" class="bi bi-check-circle-fill" viewBox="0 0 16 16">
                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
                        </svg>
                    </div>
                    <h2>Congratulations! <b id="customer_name"></b>,</h2>
                    <p>Your booking has been confirmed </p>
                    <p>Transaction id: <span class="transaction-detail"></span></p>
                    <p>Booked for: <span id="booked_for"></span></p>
                    <p>Tour cost: €<span id="tour_cost"></span></p>
                    <p>Tour detail: <span id="tour_detail"></span></p>
                    <p>Tour duration: <span id="tour_duration"></span> Minutes</p>
                    <p>Tour schedule: <span id="tour_schedule"></span></p>
                    <p>Tour Note: <span id="tour_note"></span></p>
                </div>
                <div class="text-center payment-status d-none" id="payment-processing">
                    <div class="mb-4 text-center">
                        <svg width="75px" height="75px" viewBox="0 0 1024 1024" class="icon text-text-primary" version="1.1" xmlns="http://www.w3.org/2000/svg" fill="#007bff" stroke="#007bff">
                            <g id="SVGRepo_bgCarrier" stroke-width="0"/>

                            <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"/>

                            <g id="SVGRepo_iconCarrier">

                            <path d="M511.9 183c-181.8 0-329.1 147.4-329.1 329.1s147.4 329.1 329.1 329.1c181.8 0 329.1-147.4 329.1-329.1S693.6 183 511.9 183z m0 585.2c-141.2 0-256-114.8-256-256s114.8-256 256-256 256 114.8 256 256-114.9 256-256 256z" fill="#007bff"/>

                            <path d="M548.6 365.7h-73.2v161.4l120.5 120.5 51.7-51.7-99-99z" fill="#007bff"/>
                            </g>
                        </svg>
                    </div>
                    <h2>Processing</h2>
                    <p>Your payment is being processed.</p>
                    <p class="transaction-detail"></p>
                </div>
                <div class="text-center payment-status d-none" id="payment-failed">
                    <div class="mb-4 text-center">
                        <svg version="1.1" class="text-danger" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 50 50" xml:space="preserve" width="75" height="75">
                        <circle style="fill:#D75A4A;" cx="25" cy="25" r="25"></circle>
                        <polyline style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-linecap:round;stroke-miterlimit:10;" points="16,34 25,25 34,16 
                            "></polyline>
                        <polyline style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-linecap:round;stroke-miterlimit:10;" points="16,16 25,25 34,34 
                            "></polyline>
                        </svg>
                    </div>
                    <h2>Payment failed</h2>
                    <p>You payment method is not successful. Please try again.</p>
                    <p class="transaction-detail"></p>
                </div>
            </div>
        </div>
        <div class="col-md-12 align-items-center tour-booking" id="tour-booking">
            <?php include('experiences/RCT_copy_US.php'); ?>
            <form class="mb-2 tour_booking_form" id="tour-booking-form" method="POST" enctype="multipart/form-data" action="<?= home_url().'/wp-admin/admin-post.php'; ?>">
                <input type="hidden" id="experience_id" name="experience_id">
                <input type="hidden" id="experience_date" name="experience_date">
                <input type="hidden" id="experience_time" name="experience_time">
                <input type="hidden" id="total_checkout_base_cost" name="total_checkout_base_cost">
                <input type="hidden" id="total_checkout_cost" name="total_checkout_cost">
                <input type="hidden" name="action" id="action" value="rebel_city_tour_booking" />
                <input type="hidden" id="client_secret" name="client_secret">
                <input type="hidden" id="customer_id" name="customer_id">
                <input type="hidden" id="payment_intent_id" name="payment_intent_id">
                <input type="hidden" id="booking_id" name="booking_id">
                <input type="hidden" id="country_code" name="country_code" value="353">
                <input type="hidden" name="admin_url" id="admin_url" name="admin_url" value="<?php echo admin_url( 'admin-ajax.php' ); ?>">
                <div class="row tour_booking_form">
                    <div class="experience experience-container col-md-12">
                        <h1>Choose your experience below...</h1>
                        <?php
                            $experiences = (array)json_decode($experiences);
                            foreach($experiences as $ekey1 => $eval1)
                            {   
                                $eval1_arr = (array)$eval1;
                              
                                if(isset($eval1_arr['display']) && $eval1_arr['display'] == 1)
                                {
                                    
                                    $cost = number_format(((float)((array)$eval1_arr['cost'])['eur'])/100,2,'.','');
                                
                            ?>
                                    <div class="experience-list" data-experience-id="<?= $eval1_arr['id']; ?>" data-experience-title="<?= $eval1_arr['title']; ?>" data-experience-description="<?= $eval1_arr['description']; ?>" data-experience-duration="<?= (int) $eval1_arr['duration'] * 80; ?>" data-experience-cost="<?= $cost; ?>">
                                        <h2 class="mb-0 pb-0 pt-3"><b><?= $eval1_arr['title']; ?></b></h2>
                                        <p class="cat"><span><?= (int) $eval1_arr['duration'] * 80; ?> Minutes</span> | <span>€<?= $cost; ?> EUR</span></p>
                                    </div>
                            <?php    
                                }
                            }
                        ?>
                      
                        <div class="form-btn col-md-12 mt-3 d-flex text-right">
                            <button type="button" id="first-next" class="btn submit-btn next col">
                                <h4 class="pt-2">
                                    <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                </h4>
                            </button>
                        </div>
                    </div>
                    <div class="experience experience-schedule col-md-12 d-none">
                        <?php
                            foreach($experiences as $ekey1 => $eval1)
                            {   
                                $eval1_arr = (array)$eval1;
                              
                                if(isset($eval1_arr['display']) && $eval1_arr['display'] == 1)
                                {
                                    $cost = number_format(((float)((array)$eval1_arr['cost'])['eur'])/100,2,'.','');
                                
                            ?>
                            <div class="col-md-12 experience-date-list d-none" data-experience-id="<?= $eval1_arr['id']; ?>" data-experience-title="<?= $eval1_arr['title']; ?>" data-experience-cost="<?= $cost ?>">
                                <h3 class="mb-0 pb-0 pt-3"><b><?= $eval1_arr['title']; ?></b></h3>
                                <p class="cat"><span><?= (int)$eval1_arr['duration'] * 80; ?> Minutes</span> | <span>€<?= $cost; ?> EUR</span></p>
                            </div>
                        <?php
                                }
                            }
                        ?>
                        <div class="row">
                            <div class="col-md-12 d-flex justify-content-center">
                                <h3 class="pt-3"><b>Select a date</b></h3>
                                <p><i>Monday, Tuesday, Thursday and Saturday are no tour day!</i></p>
                            </div>
                           
                            <div class="col-md-12 d-flex justify-content-center" id="datepicker"></div>
                        </div>
                        <div class="row">
<!--
                            <div class="col-md-4">
                            </div>
-->
                            <div class="form-btn col-md-12 mt-3 d-flex float-right">
                                <button type="button" class="btn submit-btn prev col mr-5">
                                    <h4 class="pt-2">
                                        <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                    </h4>
                                </button>
<!--
                            </div>
                            <div class="form-btn col-md-4 mt-3 d-flex float-right">
-->
                                 <button type="button" id="date-next" class="btn submit-btn next col">
                                    <h4 class="pt-2">
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                    </h4>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="experience experience-schedule col-md-12 d-none">
                        <?php
                            foreach($experiences as $ekey1 => $eval1)
                            {   
                                $eval1_arr = (array)$eval1;
                              
                                if(isset($eval1_arr['display']) && $eval1_arr['display'] == 1)
                                {
                                    $cost = number_format(((float)((array)$eval1_arr['cost'])['eur'])/100,2,'.','');
                                
                            ?>
                            <div class="col-md-12 experience-date-list d-none" data-experience-id="<?= $eval1_arr['id']; ?>">
                                <h3 class="mb-0 pb-0 pt-3"><b><?= $eval1_arr['title']; ?></b></h3>
                                <p class="cat"><span><?= (int) $eval1_arr['duration'] * 80; ?> Minutes</span> - <span>€<?= $cost; ?> EUR</span></p>
                            </div>
                        <?php
                                }
                            }
                        ?>
                        <div class="row">
                            <div class="col-md-12 d-flex justify-content-center">
                                <h2 class="pt-3">
                                    Select a time on <label id="selected-date"></label>
                                </h2>
                            </div>
                            <div class="col-md-12">
                                <div class="row text-center">


<!--
                                    <div class="col-md-6" id="datepicker">
                                        <div><i class="fi fi-rs-sunrise-alt" style="font-size: 2.5rem;"></i></div>
                                        <h3>Morning</h3>
                                        <div class="text-center" >
                                            <button type="button" class="btn btn-light experience-time-btn" data-experience-time="11:00 AM">11:00 AM</button>
                                        </div>
                                    </div>
-->


                                    <div class="col-md-12" id="datepicker">
                                        <div>
                                            <i class="fa fa-sun-o" style="font-size: 2.5rem;">
                                            </i>
                                        </div>
                                        <h3>Afternoon</h3>
                                        <div class="text-center" >
                                            <button type="button" class="btn btn-light experience-time-btn"data-experience-time="14:00 PM">14:00 PM</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-btn col-md-12 mt-3 d-flex float-right">
                                <button type="button" class="btn submit-btn prev col mr-5">
                                    <h4 class="pt-2">
                                        <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                    </h4>
                                </button>
                                <button type="button" id="time-next" class="btn submit-btn next col">
                                    <h4 class="pt-2">
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                    </h4>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="experience client-info col-md-12 d-none">
                        <div class="col-md-12">
                            <h2 class="mb-0 pb-0 pt-3">
                                <b>You are booking: </b>
                            </h2>
                            <h3 id="selected-experiences">
                            </h3>
                            <p class="cat">
                                <span class="tour-date-text">
                                </span> 
                                <span class="tour-time-text">
                                    2:00 PM - 3:20 PM 
                                </span> 
                                <span class="tour-time-zone">
                                    Europe/Dublin 
                                </span> - €<span id="total-cost"></span>EUR
                            </p>
                        </div>

<!--
                                                           
                        <div class="col-md-12 pt-3" style="margin:20px 0;">
                            <label class="strong-label" >
                                <strong >Step-by-step meeting point directions will be sent to your phone upon booking.</strong>
                            </label>
                        </div>

-->
                                                           
                        <div class="row">
                        <div class="col-md-12 mb-5" style="margin:20px 0;">                                                           
                                <span class="input-icon"><i class="fi fi-sr-man-head flat-icon"></i></span>
                                
                                <input type="text" id="tour-customer-name" name="customer_name" class="tour-customer-input col" placeholder="Name">
                            </div>
                        </div>

                            <div class="row">
                        <div class="col-md-12 mb-5" style="margin:20px 0;">
                                                           <span class="input-icon">
                                    <i class="fi fi-sr-envelope flat-icon"></i>
                                </span>
                                
                                <input type="email" id="tour-customer-email" name="customer_email" class="tour-customer-input col" placeholder="Email">
                            </div>
                        </div>
                            <div class="row">
                              <div class="col-md-12 mb-3" style="margin:20px 0;">
                                <input type="text" id="tour-customer-phone" name="customer_phone" class="tour-customer-input col" placeholder="Phone">
                            </div>
                        </div>
                                                           
<!--                                                           
                            <div class="row">
                             <div class="col-md-12 mb-5 ml-2" style="margin: 20px 0;" >

                                <label class="pl-2">
                                    <input type="checkbox" id="tour-customer-phone-reminder" class="form-group checkboxes strong-label" name="appointment_reminder">
                                    Please remind me about this appointment by text at this number
                                </label>
                            </div>
                        </div>
-->

                                                           
                            <div class="row">
                        <div class="col-md-12 mb-5 ml-2">
                                                           <label class="pl-2">
                                    Booking for 
                                </label>
                                <select class="form-control form-select form-select-lg mb-3" aria-label=".form-select-lg" id="ticket_qty" name="ticket_qty">
                                    <option value="1" selected>1 Person</option>
                                    <?php 
                                        for($i = 2; $i<=30; $i++)
                                        {
                                            echo '<option value="'.$i.'">'.$i.' Persons</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                            <div class="row">
                        <div class="col-md-12 mb-2" style="margin:40px 0;" >
                                                           <span class="input-icon">
                                <i class="fi fi-ss-notebook flat-icon"></i>
                                </span>
                                <input type="text" id="tour-customer-note" name="tour_customer_note" class="tour-customer-input col mb-5 mt-5" placeholder="Note to tour guide">
                                <label class="pl-2 pt-3">
                                    <h3>
                                        <strong>
                                            <i class="fi fi-bs-check pr-2 strong-label"></i>Here's the Rebel City Tour liability, release and waiver, in human language.
                                        </strong> 
                                    </h3>
                                    <p class="text-muted">
                                        1. Here's the Rebel City Tour liability, release and waiver, in human language 2. If you participate in a Rebel
                                        City Tour, walking tour, you are agreeing to the following: 3. I am taking this tour voluntarily and am in good
                                        health. 4. I accept personal responsibility for anything that happens to me, like if I get hurt or lose
                                        something. 5. If something bad happens to me during the tour, it's not Rebel City Tour's fault. 6. I release
                                        Rebel City Tour and anyone working for it, including contractors, from any claims or liability for damages. 7.
                                        If I agreed to it, Rebel City Tour can take photos and video during the walking tour and can use my image,
                                        likeness, recordings of me in any format to promote their tours or any other way they like without having to
                                        pay me anything for it, ever. 8. I understand there are risks to walking in public.
                                    </p>
                                </label>

                            </div>
                                <div class="row">
                                                           <div class="col-md-12 mb-5">

                                    <label class="pl-2" >
                                        <input type="checkbox" id="tour-customer-tnc" name="tnc_agree"  class="form-group checkbox-round checkboxes" class="strong-label">
                                        I have read and agree to the Terms and Conditions and Indemnity Waiver
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mb-5 ml-2">

                                <label class="pl-2">
                                    <input type="checkbox" id="tour-customer-newsletter" name="newsletter_subscription" class="form-group checkboxes strong-label">
                                                           Let&rsquo;s stay in touch? Subscribe to our email newsletter.
                                </label>
                            </div>
                        </div>
                            <div class="row">
                                                               <div class="col-md-12 mb-5 ml-2">

                                                                                                                       <table>
                                                                                                                        <tr>
                                                                                                                         <td>
                                                                                                                            <b>Price</b>

                                                                                                                         </td>
                                                                                                                         <td>

                                                                                                                            €<span class="total-checkout-cost"></span>

                                                                                                                       </td>

                                                                                                                       </tr>
                                                                                                                       <tr>


                             <td>

                                <b>Payment Method</b>
                                                                                                                       </td>

                                                                                                                       <td>

                                <input type="radio" id="payment-method" name="payment_method" checked> Rebel City Tour Payment

                                                                                                                       <p class="text-muted">

                                                                                                                       Pay with credit card

                                                                                                                       </p>
                         
                                                                                                                       </td>
                                                                                                                       </tr>

                                                                                                                       <tr>

                                                                                                                       <td>          <b>Refund Policy</b>
                                                                                                                       </td>

                                                                                                                       <td>

                                                                                                                       24-hrs cancellation notice refund                                                                                                                                                    </td>

                                                                                                                       </tr>
                                                                                                                       </table>
                                                                                                                       
                            </div>
                                                                                                                  </div>
                        <div class="row pb-3">
                            <div class="container" id="paymentElement">
                                <!--Stripe.js injects the Payment Element-->
                            </div>
                        </div>
                        <div class="row pb-3">
                            <div class="form-btn col-md-12 mt-3 d-flex float-right">
                                <button type="button" id="confirm_btn" class="btn text-light" style="background-color:#86a214 !important; font-weight:bold">
                                    <h4 class="pt-2" id="btn-text">
                                        Confirm
                                    </h4>
                                </button>
                                <button type="submit" id="pay" class="btn text-light d-none" style="background-color:#86a214 !important; font-weight:bold">
                                    <h6 class="pt-2" id="btn-text">
                                        Pay Now
                                    </h6>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
</html>
