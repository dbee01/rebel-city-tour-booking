<?php
/**
 * template name: Calendar Events
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Rebel City Tour Booking
 */
require_once dirname(__DIR__).'/vendor/zapcallib.php';

$title = "Rebel City Tour Booking";
$description = isset($_GET['description']) && !empty($_GET['description']) ? $_GET['description'] : '';
$event_start = isset($_GET['event_start']) && !empty($_GET['event_start']) ? $_GET['event_start'] : '';
$event_end = isset($_GET['event_end']) && !empty($_GET['event_end']) ? $_GET['event_end'] : '';
// date/time is in SQL datetime format
$event_start = "2020-01-01 12:00:00";
$event_end = "2020-01-01 13:00:00";

// create the ical object
$icalobj = new ZCiCal();

// create the event within the ical object
$eventobj = new ZCiCalNode("VEVENT", $icalobj->curnode);

// add title
$eventobj->addNode(new ZCiCalDataNode("SUMMARY:" . $title));

// add start date
$eventobj->addNode(new ZCiCalDataNode("DTSTART:" . ZCiCal::fromSqlDateTime($event_start)));

// add end date
$eventobj->addNode(new ZCiCalDataNode("DTEND:" . ZCiCal::fromSqlDateTime($event_end)));

// UID is a required item in VEVENT, create unique string for this event
// Adding your domain to the end is a good way of creating uniqueness
$uid = date('Y-m-d-H-i-s');
$eventobj->addNode(new ZCiCalDataNode("UID:" . $uid));

// DTSTAMP is a required item in VEVENT
$eventobj->addNode(new ZCiCalDataNode("DTSTAMP:" . ZCiCal::fromSqlDateTime()));

// Add description
$eventobj->addNode(new ZCiCalDataNode("Description:" . ZCiCal::formatContent(
        "This is a simple event, using the Zap Calendar PHP library. " .
        "Visit http://icalendar.org to validate icalendar files.")));

// write iCalendar feed to stdout
echo $icalobj->export();