$(document).ready(function(e)
{

    // Toggle to 0 to switch to live. Don't forget the payment class
    let test = 0; 

    if(test == 1)
    {
        var STRIPE_PUBLISHABLE_KEY = 'pk_test_51LqX9cLX928MCAsH80ZclPMmj8Xe1eHAY79be8Ddk0xs2LUR3rRWnZzhraqAON1PCWyU5roP5ih9WuW2itGFQ7Mz00UlDf5zgo';
    }
    else
    {
        var STRIPE_PUBLISHABLE_KEY = 'pk_live_51LqX9cLX928MCAsHT11AdJnKPEF1vDjoUPbuLIBcOGsd2GYeOVeXr3dqD1dyeSA9UZHVeNRbGQvxawaNaV8mwSoA004FXlixWZ';
    }

    $("#loader").hide();

    /* Call date picker for Tour Schedule Selection */
    $( "#datepicker" ).datepicker
    (
        {   
            minDate: new Date(),
            dateFormat: "dd-mm-yy", 
            monthNames: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
            onSelect: function()
            {
                var date = $('#datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
                var months = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
                var selectedMonthName = months[$("#datepicker").datepicker('getDate').getMonth()];
                var selectedDay = $("#datepicker").datepicker('getDate').getDate();
                var selectedYear = $("#datepicker").datepicker('getDate').getFullYear();
                experience_date = selectedMonthName+' '+selectedDay+', '+selectedYear;
                $("#experience_date").val(experience_date);
                $("#selected-date").text(experience_date);
                $(".tour-date-text").text(experience_date);
            },
            beforeShowDay: function(date) 
            {
                var show = true;
                if(date.getDay()==1||date.getDay()==2 || date.getDay()==4 || date.getDay()==6) show=false
                return [show];
             }
        } 
    );
    var date = $('#datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
    var months = [ "January", "February", "March", "April", "May", "June", 
    "July", "August", "September", "October", "November", "December" ];
    var selectedMonthName = months[$("#datepicker").datepicker('getDate').getMonth()];
    var selectedDay = $("#datepicker").datepicker('getDate').getDate();
    var selectedYear = $("#datepicker").datepicker('getDate').getFullYear();
    experience_date = selectedMonthName+' '+selectedDay+', '+selectedYear;
    $("#experience_date").val(experience_date);
    $("#selected-date").text(experience_date);
    $(".tour-date-text").text(experience_date);
    
    /* Update ticket quantity */
    $(document).on("change","#ticket_qty",function(etb)
    {   
        total_price = parseFloat($(this).val())*$("#total_checkout_base_cost").val();
        
	    total_price_2_decimals = total_price.toFixed(2);
        
        $("#total_checkout_cost").val(total_price_2_decimals);
        $(".total-checkout-cost").text(total_price_2_decimals);
        
        // Price with decimals for UI 
        $("#total-cost").text(total_price_2_decimals);
    });

    /* Set experience time */
    $(document).on("click",".experience-time-btn",function(etb)
    {   
        time_btn = $(this);
        $("#experience_time").val(time_btn.attr("data-experience-time"));
        $(".active").removeClass(".active");
        time_btn.addClass("active");
        $(".tour-time-text").text(time_btn.attr("data-experience-time"));
    });
    /* Navigate to the next page */
    $(document).on("click",".next",function(e1)
    {   
        curr_ref = $(this);
        if($(this).attr('id') == 'time-next' && !$.trim($("#experience_time").val()))
        {
            toastr.error('Please choose your preferred time slot!');
            return false;
        }
        
        if($("#experience_id").val())
        {   
            if($(this).attr('id') == 'first-next')
            {   
                data_experience_cost = 0;
                $('.experience-date-list').each(function(i, obj) 
                {   
                    experience_ids = $("#experience_id").val().split(',');
                    if(experience_ids.includes($(obj).attr('data-experience-id')))
                    {
                        console.log($(obj).attr('data-experience-title'));
                        if($(obj).hasClass('d-none'))
                        {
                            $(obj).removeClass('d-none');
                        }
                        selected_experiences = $.trim($("#selected-experiences").text());
                        if(selected_experiences)
                        {   
                            
                            if($(obj).attr('data-experience-title') !== undefined)
                            {   
                                ticket_qty = $("#ticket_qty").val();
                                data_experience_cost = parseInt(ticket_qty)*parseFloat(data_experience_cost+parseFloat($(obj).attr('data-experience-cost')));
                                $("#selected-experiences").text($("#selected-experiences").text()+','+$(obj).attr('data-experience-title'));
                                $("#total-cost").text(data_experience_cost);
                                $("#total_checkout_cost").val(data_experience_cost);
                                $(".total-cost-checkout").text(data_experience_cost);
                                $("#total_checkout_base_cost").val(data_experience_cost);
                            }
                        }
                        else
                        {   
                            if($(obj).attr('data-experience-title') !== undefined)
                            {
                                ticket_qty = $("#ticket_qty").val();
                                data_experience_cost = parseInt(ticket_qty)*parseFloat(data_experience_cost+parseFloat($(obj).attr('data-experience-cost')));
                                $("#selected-experiences").text($(obj).attr('data-experience-title')); 
                                $("#total-cost").text(data_experience_cost);
                                $("#total_checkout_cost").val(data_experience_cost);
                                $(".total-checkout-cost").text(data_experience_cost);
                                $("#total_checkout_base_cost").val(data_experience_cost);
                            }   
                        }
                    }
                });
            }
            if(curr_ref.parent().parent().hasClass("experience"))
            {
                curr_ref.parent().parent(".experience").animate({left: '-150%'}, 500 );
            }
            else
            {
                curr_ref.parent().parent().parent(".experience").animate({left: '-150%'}, 500 );
            }
            if(curr_ref.parent().parent().hasClass("experience"))
            {
                setTimeout(
                    function() 
                    {   
                        curr_ref.parent().parent(".experience").addClass("d-none");
                        curr_ref.parent().parent(".experience").next().removeClass('d-none');
                    }, 
                300);
            }
            else
            {
                setTimeout(
                    function() 
                    {   
                        curr_ref.parent().parent().parent(".experience").addClass("d-none");
                        curr_ref.parent().parent().parent(".experience").next().removeClass('d-none');
                    }, 
                300);
            }
        }
        else
        {
            toastr.error('Please choose at least one experience!');
        }
    }); 
    
    /* Navigate back to the previous page */
    $(document).on("click",".prev",function(e2)
    {    
        curr_ref = $(this);
        if(curr_ref.parent().parent().hasClass("experience"))
        {
            curr_ref.parent().parent(".experience").addClass('d-none');
            curr_ref.parent().parent(".experience").prev().removeClass('d-none');
        }
        else{
            curr_ref.parent().parent().parent(".experience").addClass('d-none');
            curr_ref.parent().parent().parent(".experience").prev().removeClass('d-none');
        }
        if(curr_ref.parent().parent().hasClass("experience")){
            setTimeout(
                function() 
                {   
                    curr_ref.parent().parent(".experience").prev(".experience").animate({left: '0%'},500);
                }, 
            200);
        }
        else{
            setTimeout(
                function() 
                {   
                    curr_ref.parent().parent().parent(".experience").prev(".experience").animate({left: '0%'},500);
                }, 
            200);
        }
    });
    
    /* Save selected experience detail in hidden input field for further processing */
    $(document).on("click",".experience-list",function(el1)
    {  
        $(this).toggleClass('experience-active');
        if($(this).hasClass("experience-active"))
        {
            data_experience_id = $(this).attr('data-experience-id');
            existing_data_experience_id = $("#experience_id").val();
            
            if(existing_data_experience_id.indexOf(data_experience_id) == -1)
            {   
                // console.log(existing_data_experience_id.indexOf(data_experience_id));
                if(existing_data_experience_id)
                {
                    $("#experience_id").val(existing_data_experience_id+','+data_experience_id);
                }
                else
                {
                    $("#experience_id").val(data_experience_id);
                }
            }
        }
        else
        {
            data_experience_id = $(this).attr('data-experience-id');
            existing_data_experience_id = $("#experience_id").val();
            
            // console.log(existing_data_experience_id.indexOf(data_experience_id));
            if(existing_data_experience_id.indexOf(data_experience_id) != -1)
            {   
                $("#experience_id").val(existing_data_experience_id.replace(data_experience_id,''));
                $("#experience_id").val($("#experience_id").val().replace(/^,|,$|(,)+/g, '$1'));
            }
        }
    });


    // Create an instance of the Stripe object and set your publishable API key
    const stripe = Stripe(STRIPE_PUBLISHABLE_KEY);

    // Define card elements
    let elements;


    // Get payment_intent_client_secret param from URL
    clientSecretParam = new URLSearchParams(window.location.search).get(
        "payment_intent_client_secret"
    );

    // Check whether the payment_intent_client_secret is already exist in the URL
    
    // Attach an event handler to payment form
    $(document).on("click",'#confirm_btn',async function(e31)
    {   
        $("#loader").show();
        e31.preventDefault();
        if(!$('#tour-customer-tnc').is(':checked')) 
        {   
            $("#loader").hide();
            e31.preventDefault();
            toastr.error('Please agree to the terms and conditions');
            return;
        }
        if(!$('#tour-customer-name').val()) 
        {   
            $("#loader").hide();
            e31.preventDefault();
            toastr.error('Please enter your full name');
            return;
        }
        if(!$('#tour-customer-phone').val()) 
        {   
            $("#loader").hide();
            e31.preventDefault();
            toastr.error('Please enter your contact number');
            return;
        }
        if(!$('#tour-customer-email').val()) 
        {   
            $("#loader").hide();
            e31.preventDefault();
            toastr.error('Please enter your email');
            return;
        }
        else
        {   
            // Check whether the payment_intent_client_secret is already exist in the URL
            $("#loader").hide();
            if(!clientSecretParam)
            {   
                // Create an instance of the Elements UI library and attach the client secret
                initialize();
            }
        }
    });
    
    // Check the PaymentIntent creation status
    checkStatus();

    $(document).on("submit",'#tour-booking-form',async function(e31)
    {
        e31.preventDefault();
        if(!$('#tour-customer-tnc').is(':checked')) 
        {   
            e31.preventDefault();
            toastr.error('Please agree to the terms and conditions');
            return;
        }
        if(!$('#tour-customer-name').val()) 
        {   
            e31.preventDefault();
            toastr.error('Please enter your full name');
            return;
        }
        if(!$('#tour-customer-phone').val()) 
        {   
            e31.preventDefault();
            toastr.error('Please enter your contact number');
            return;
        }
        if(!$('#tour-customer-email').val()) 
        {   
            e31.preventDefault();
            toastr.error('Please enter your email');
            return;
        }
        else
        {   
            e31.preventDefault();
            let customer_name = $('#tour-customer-name').val();
            let customer_email = $('#tour-customer-email').val();
            let customer_phone = '+'+$('#country_code').val()+$('#tour-customer-phone').val();
            let experience_date = $('#experience_date').val();
            let payment_intent_id = $('#payment_intent_id').val();
            let experience_time = $('#experience_time').val();
            let appointment_reminder = $('#tour-customer-phone-reminder').val();
            let newsletter_subscription = $('#tour-customer-newsletter').val();
            let tour_note = $('#tour-customer-note').val();
            let ticket_qty = $('#ticket_qty').val();

            let admin_url = $('#admin_url').val()+'?action='+'rebel_city_tour_booking';
            
            let requestDataTourBooking = 
            {
                action: "rebel_city_tour_booking",
                payment_intent_id: payment_intent_id, 
                experience_id:$("#experience_id").val(),
                customer_name: customer_name, 
                customer_phone: customer_phone, 
                customer_email: customer_email,
                experience_date: experience_date,    
                experience_time: experience_time,
                appointment_reminder: appointment_reminder,
                tour_note: tour_note,
                ticket_qty: ticket_qty,
                newsletter_subscription: newsletter_subscription,
            };
            let customer_data = await fetch(admin_url, {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify({ requestDataTourBooking }),
            }).then(response => {
                response = response.json();
                return response;
            });
            id = customer_data.id;
            customer_id = customer_data.customer_id;
            booking_id = customer_data.booking_id;
            $("#customer_id").val(customer_id);
            $("#booking_id").val(booking_id);
            console.log(customer_data);
            let { error } = await stripe.confirmPayment({
                elements,
                confirmParams: {
                    // payment completion page
                    return_url: window.location.href+'?customer_id='+customer_id+'&client_secret='+$("#client_secret").val()+'&booking_id='+$("#booking_id").val(),
                },
            });
            if (error.type === "card_error" || error.type === "validation_error") 
            {
                toastr.error(error.message);
            }
            else 
            {
                toastr.error("An unexpected error occured.");
            }
        }
    });

    // Fetch a payment intent and capture the client secret
    let payment_intent_id;
    async function initialize() 
    {  
        let admin_url = $("#admin_url").val()+'?action='+'create_payment_intent';
        let requestData = {
            action:'create_payment_intent',
            experience_ids:$("#experience_id").val(),
            ticket_qty:$("#ticket_qty").val(),
            nonce: 'test nonce'
        };
        let data = await fetch(admin_url, {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ requestData}),
        }).then(response => {
            response = response.json();
            console.log(response);
            return response;
        });
        const appearance = {
            theme: 'stripe',
            rules: {
                '.Label': {
                    fontWeight: 'bold',
                    textTransform: 'uppercase',
                }
            }
        };

        response_status =  data.status;
        message =  data.message;
        if(response_status == 200)
        {   
            toastr.success(message);
            $("#btn-text").text('Pay Now');
            id =  data.id;
            clientSecret = data.clientSecret;
            $("#client_secret").val(clientSecret);
            elements = stripe.elements({ clientSecret, appearance });
            
            const paymentElement = elements.create("payment");
            paymentElement.mount("#paymentElement");
            
            payment_intent_id = id;
            $("#payment_intent_id").val(payment_intent_id);
            $("#confirm_btn").addClass("d-none");
            $("#pay").removeClass("d-none");
        }
        else
        {
            toastr.error(message);
        }
    }

    // Fetch the PaymentIntent status after payment submission
    async function checkStatus() 
    {
        clientSecret = new URLSearchParams(window.location.search).get(
            "payment_intent_client_secret"
        );
        
        customerID = new URLSearchParams(window.location.search).get(
            "customer_id"
        );

        bookingID = new URLSearchParams(window.location.search).get(
            "booking_id"
        );
        
        if (!clientSecret) 
        {
            return;
        }
        
        let { paymentIntent } = await stripe.retrievePaymentIntent(clientSecret);
        // console.log(paymentIntent);
        if (paymentIntent) 
        {
            switch (paymentIntent.status) 
            { 
                case "succeeded":
                    // Post the transaction info to the server-side script and redirect to the payment status page
                    
                    let admin_url = $("#admin_url").val()+'?action='+'update_payment';
                    let requestPaymentData = {
                        action:'update_payment',
                        payment_intent:paymentIntent,
                        customer_id:customerID,
                        booking_id:bookingID,
                    };
                    let payment_response = await fetch(admin_url, {
                        method: "POST",
                        headers: { "Content-Type": "application/json" },
                        body: JSON.stringify({ requestPaymentData}),
                    }).then(response => {
                        response = response.json();
                        console.log(response);
                        return response;
                    }).then(data => {
                        if (data.payment_txn_id) 
                        {
                            $(".transaction-detail").text(data.payment_txn_id);
                            $("#customer_name").text(data.customer_name);
                            $("#tour_cost").text(data.paid_amount);
                            $("#tour_detail").text(data.experience_detail);
                            $("#tour_duration").text(data.experience_duration.replace(',', '+'));
                            $("#booked_for").text(data.ticket_qty+' Person');
                            $("#tour_note").text(data.tour_note);
                            $("#tour_schedule").text(data.tour_date+' '+data.tour_time);
                            $("#payment-confirmation").removeClass('d-none');
                            $("#payment-completed").removeClass('d-none');
                            $("#tour-booking").addClass('d-none');
                            toastr.success(data.message);
                            // Clear the previous ecommerce object.
                            // remove this, send event object on test and live
			    // if(test == 0)
                            // {   
                                if(dataLayer.length > 0)
                                {
                                     dataLayer.push({ ecommerce: null });  
                                    	// we're pushing this event with GTM, no need to push manually/programmatically
					dataLayer.push({
                                          event: "purchase",
                                          ecommerce: 
                                          {
                                            transaction_id: data.payment_txn_id,
                                            value: data.paid_amount,
                                            tax: 0,
                                            shipping: 0,
                                            currency: "EUR",
                                            coupon: "",
                                            items: 
                                            [
                                                {
                                                    item_id: "rwtc",
                                                    item_name: data.experience_detail,
                                                    affiliation: "Rebel City Tour",
                                                    coupon: "SUMMER_FUN",
                                                    discount: 3.33,
                                                    index: 1,
                                                    item_brand: "RebelCityTour",
                                                    item_category: "Experience",
                                                    item_list_id: "rebel_walking_tour",
                                                    location_id: "ChIJIQBpAG2ahYAR_6128GcTUEo",
                                                    price: $("#total_checkout_base_cost").val(),
                                                    promotion_id: "P_12345",
                                                    promotion_name: "Summer Sale",
                                                    quantity: ticket_qty
                                                }
                                           ]
                                        }
                                    });
                                }
                                
                            // }
                        } 
                        else 
                        {
                            toastr.error(data.message);
                            setReinit();
                        }
                    }).catch(console.error);
                break;
                case "processing":
                    $(".transaction-detail").text(data.payment_txn_id);
                    $("#payment-confirmation").removeClass('d-none');
                    $("#payment-processing").removeClass('d-none');
                    $("#tour-booking").addClass('d-none');
                    toastr.info("Your payment is processing.");
                    setReinit();
                break;
                case "requires_payment_method":
                    $(".transaction-detail").text(data.payment_txn_id);
                    $("#payment-confirmation").removeClass('d-none');
                    $("#payment-failed").removeClass('d-none');
                    $("#tour-booking").addClass('d-none');
                    setReinit();
                    toastr.error("Your payment was not successful, please try again.");
                    setReinit();
                break;
                default:
                    toastr.error("Something went wrong.");
                    setReinit();
                break;
            }
        } 
        else 
        {
            toastr.error("Something went wrong.");
            setReinit();
        }
    }
     
    // var input = document.querySelector("#tour-customer-phone");
    //   window.intlTelInput(input, {
    //     // allowDropdown: false,
    //     // autoInsertDialCode: true,
    //     // autoPlaceholder: "off",
    //     // dropdownContainer: document.body,
    //     // excludeCountries: ["us"],
    //     // formatOnDisplay: false,
    //     // geoIpLookup: function(callback) {
    //     //   fetch("https://ipapi.co/json")
    //     //     .then(function(res) { return res.json(); })
    //     //     .then(function(data) { callback(data.country_code); })
    //     //     .catch(function() { callback("us"); });
    //     // },
    //     // hiddenInput: "full_number",
    //     // initialCountry: "auto",
    //     // localizedCountries: { 'de': 'Deutschland' },
    //     // nationalMode: false,
    //     // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
    //     // placeholderNumberType: "MOBILE",
    //     // preferredCountries: ['cn', 'jp'],
    //     // separateDialCode: true,
    //     // showFlags: false,
    //     utilsScript: "https://cdn.jsdelivr.net/npm/intl-tel-input@18.1.1/build/js/utils.js",
    // });
    
    var input = $("#tour-customer-phone");
    input.intlTelInput();
    
    $("#tour-customer-phone").on("countrychange", function () {
        var SelectedCountry = $("#tour-customer-phone").intlTelInput("getSelectedCountryData");
        $("#country_code").val(SelectedCountry.dialCode);
   });
});
