<?php

$experiences = '[
    {
        "id": "1",
        "display": "1",
        "title": "Rebel city tour; an historical walking tour of Cork city",
        "slug": "walking-tour-cork-city",
        "description": "Why is Cork called the Rebel county? Immerse yourself in the culture and history of Cork city by coming along on a 90 min walking tour of the city. Hear about tales from the Irish War of Independence and walk in the footsteps of the plucky Volunteer fighters, taking on the might of an Empire to win Ireland&lsquo;s freedom.",
        "description_mi": "I&lsquo;ll discover why Cork is called the Rebel county. And I&lsquo;ll be immersed in the culture and history of Cork city by taking a 90 min walking tour of the city. I&lsquo;ll hear tales from the Irish War of Independence and I&lsquo;ll walk in the footsteps of the plucky Volunteer fighters, taking on the might of an Empire to win Ireland&lsquo;s freedom.",
        "image": "/assets/img/experience/experience-5.png",
        "duration": 1.2,
        "cost":
        {
            "eur": 2199,
            "usd": 2199
        }
    }
]';

