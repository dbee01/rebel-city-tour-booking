<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
require_once dirname(__DIR__).'/vendor/autoload.php';
require_once dirname(__DIR__).'/vendor/icalendar/zapcallib.php';

// Toggle to 0 to switch to live. Don't forget custom.js
$test = 0; 

if($test == 1)
{
  define('STRIPE_API_KEY', 'sk_test_51LqX9cLX928MCAsHMJWXNJb98tlFyIvz1OQ2o1uLQJF91vsH1u8evWBVfii1DSRWSJJVhQTL8N0Equ4J2hj0chHd00F02bMwwl'); 
  define('STRIPE_PUBLISHABLE_KEY', 'pk_test_51LqX9cLX928MCAsH80ZclPMmj8Xe1eHAY79be8Ddk0xs2LUR3rRWnZzhraqAON1PCWyU5roP5ih9WuW2itGFQ7Mz00UlDf5zgo');
}
else
{
  define('STRIPE_API_KEY', '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'); 
  define('STRIPE_PUBLISHABLE_KEY', 'pk_live_51LqX9cLX928MCAsHT11AdJnKPEF1vDjoUPbuLIBcOGsd2GYeOVeXr3dqD1dyeSA9UZHVeNRbGQvxawaNaV8mwSoA004FXlixWZ');
}
/* Class to handle tour booking payment including create, read, update and delete */
class TourBookingPayment
{ 
  function create_tour_booking_payment_intent($experience_ids,$ticket_qty)
  { 
    \Stripe\Stripe::setApiKey(STRIPE_API_KEY); 
    $experience_data = $this->experience_data($experience_ids);
    $total_price = 0;
    $all_titles = '';
    foreach($experience_data as $edk1 => $edv1)
    { 
      $total_price = $total_price+$edv1['cost'];
      if(empty($all_titles))
      {
        $all_titles = $edv1['title'];
      }
      else
      {
        $all_titles = $all_titles.','.$edv1['title'];
      }
    }
    $total_price = (int)$ticket_qty*(float)$total_price;
    try { 
      // Create PaymentIntent with amount and currency 
      $paymentIntent = \Stripe\PaymentIntent::create([ 
        'amount' => $total_price, 
        'currency' => 'EUR', 
        'description' => $all_titles, 
        'payment_method_types' => [ 
          'card' 
        ] 
      ]); 
    
      $output = [ 
        'status' => 200,
        'id' => $paymentIntent->id, 
        'clientSecret' => $paymentIntent->client_secret,
        'message' => 'Payment intent has been created',
      ]; 
    
      // echo json_encode($output); 
    } 
    catch (Error $e) 
    { 
      // http_response_code(500); 
      // echo json_encode(['error' => $e->getMessage()]); 
      $output = [
        'status' => 500,
        'message' => $e->getMessage(),
      ];
    } 
    return $output; 
  }

  function create_tour_booking_customer($tour_data)
  {
    \Stripe\Stripe::setApiKey(STRIPE_API_KEY); 
    $payment_intent_id = !empty($tour_data['payment_intent_id'])?$tour_data['payment_intent_id']:''; 
    $name = !empty($tour_data['customer_name'])?$tour_data['customer_name']:''; 
    $email = !empty($tour_data['customer_email'])?$tour_data['customer_email']:''; 
     
    // Add customer to stripe 
    try
    {   
      $customer = \Stripe\Customer::create(array(  
        'name' => $name,  
        'email' => $email 
    ));  
    }
    catch(Exception $e) 
    {   
      $api_error = $e->getMessage();   
    } 
     
    if(empty($api_error) && $customer){ 
      try 
      { 
        // Update PaymentIntent with the customer ID 
        $paymentIntent = \Stripe\PaymentIntent::update($payment_intent_id, [ 
          'customer' => $customer->id 
        ]); 
      } 
      catch (Exception $e) 
      {  
        // log or do what you want 
      } 
      $output = [ 
        'status' => 200,
        'id' => $payment_intent_id, 
        'booking_id' => $tour_data['booking_id'],
        'customer_id' => $customer->id,
        'message' => 'Customer has been created',
      ]; 
    }
    else
    { 
      $output = [
        'status' => 500,
        'message' => $api_error,
      ];
    } 
    return $output; 
  }

  function tour_booking_payment($transaction_data)
  {
    \Stripe\Stripe::setApiKey(STRIPE_API_KEY); 
    global $wpdb;
    $payment_intent = !empty($transaction_data['payment_intent'])?(object)$transaction_data['payment_intent']:''; 
    $customer_id = !empty($transaction_data['customer_id'])?$transaction_data['customer_id']:''; 
    $booking_id = !empty($transaction_data['booking_id'])?$transaction_data['booking_id']:''; 
  
    // Retrieve customer info 
    try 
    {   
      $customer = \Stripe\Customer::retrieve($customer_id);  
    }
    catch(Exception $e) 
    {   
      $api_error = $e->getMessage();   
    } 
    // Check whether the charge was successful 
    if(!empty($payment_intent) && $payment_intent->status == 'succeeded')
    { 
      // Transaction details  
      $transaction_id = $payment_intent->id; 
      $paid_amount = ($payment_intent->amount)/100; 
      $paid_currency = $payment_intent->currency; 
      $payment_status = $payment_intent->status; 
        
      $customer_name = $customer_email = ''; 
      if(!empty($customer))
      { 
        $customer_name = !empty($customer->name)?$customer->name:''; 
        $customer_email = !empty($customer->email)?$customer->email:''; 
      } 
      
      $payment_id = ''; 
      $table_name = $wpdb->prefix . "tour_payments";
      $payment_id = $wpdb->get_results("SELECT `id` FROM $table_name WHERE `txn_id` = '$transaction_id';");
      
      if(!empty($payment_id))
      { 
        $payment_id = $payment_id; 
      }
      else
      { 
        $wpdb->insert($table_name, array(
          'booking_id' => $booking_id,
          'txn_id' => $transaction_id,
          'paid_amount' => $paid_amount,
          'paid_amount_currency' => $paid_currency, 
          'payment_status' => $payment_intent->status,
          'customer_name' => $customer_name,
          'customer_email' => $customer_email, 
        ));
        $payment_id = $wpdb->insert_id;   
      
      } 
      $booking_table = $wpdb->prefix . "tour_bookings";
      $wpdb->query($wpdb->prepare("UPDATE $booking_table SET `payment_status`=1,`stripe_customer_id`='$customer_id',`experience_cost`=$paid_amount WHERE `id`='$booking_id'"));

      $booking_result = $wpdb->get_results( "SELECT * FROM `$booking_table` WHERE `id` = '$booking_id'" );

      foreach ($booking_result as $booking_data)
      {
        $experience_detail = $booking_data->experience_detail;
        $experience_duration = $booking_data->experience_duration;
        $customer_phone = $booking_data->customer_phone;
        $tour_date = $booking_data->tour_date;
        $tour_time = $booking_data->tour_time;
        $tour_note = $booking_data->tour_note;
        $ticket_qty = $booking_data->ticket_qty;
      }

      $output = [ 
        'status' => 200,
        'payment_txn_id' => base64_encode($transaction_id),
        'paid_amount' => $paid_amount,
        'paid_amount_currency' => $paid_currency, 
        'customer_name' => $customer_name,
        'customer_email' => $customer_email, 
        'experience_detail' => $experience_detail,
        'experience_duration' => $experience_duration,
        'tour_date' => $tour_date,
        'tour_time' => $tour_time,
        'tour_note' => $tour_note,
        'ticket_qty' => $ticket_qty,
        'message' => 'Payment completed!',
      ]; 
    }
    else
    { 
      $output = [
        'status' => 500,
        'message' => 'Transaction has been failed!',
      ];
    }
    
    // Generate and send ICS file on button link
    $title = "Rebel City Tour Booking";
    $description = $experience_detail;
    $formatted_tour_start_date = date('Y-m-d', strtotime($tour_date));
    $formatted_tour_start_time = date('H:i:s', strtotime($tour_time));

    $formatted_tour_end_date = $formatted_tour_start_date;
    $formatted_tour_end_time = date('H:i:s', strtotime($tour_time)+4800);
    $event_start = $formatted_tour_start_date.' '.$formatted_tour_start_time;
    $event_end = $formatted_tour_end_date.' '.$formatted_tour_end_time;

    // create the ical object
    $icalobj = new ZCiCal();

    // create the event within the ical object
    $eventobj = new ZCiCalNode("VEVENT", $icalobj->curnode);

    // add title
    $eventobj->addNode(new ZCiCalDataNode("SUMMARY:" . $title));

    // add start date
    $eventobj->addNode(new ZCiCalDataNode("DTSTART:" . ZCiCal::fromSqlDateTime($event_start)));

    // add end date
    $eventobj->addNode(new ZCiCalDataNode("DTEND:" . ZCiCal::fromSqlDateTime($event_end)));

    // UID is a required item in VEVENT, create unique string for this event
    // Adding your domain to the end is a good way of creating uniqueness
    $uid = $transaction_id;
    $eventobj->addNode(new ZCiCalDataNode("UID:" . $uid));

    // DTSTAMP is a required item in VEVENT
    $eventobj->addNode(new ZCiCalDataNode("DTSTAMP:" . ZCiCal::fromSqlDateTime()));

    // Add description
    $eventobj->addNode(new ZCiCalDataNode("Description:" . ZCiCal::formatContent(
     $description)));

    // write iCalendar feed to stdout
    $ics_data = $icalobj->export();
    $ics_file_name = $uid.'.ics';
    $ics_file = dirname(__DIR__).'/ics/'.$ics_file_name;

    // if (!file_exists($ics_file))
    // {
    //  touch($ics_file);
    // }
    $fp = fopen($ics_file, 'w');
    $contents = $ics_data;   
    file_put_contents($ics_file, $contents);

    // fwrite($fp, $content);
    fclose($fp);
    chmod($ics_file, 0777); 
    $ics_file_url = str_replace(' ', '',plugin_dir_url(__DIR__).'ics/'.$ics_file_name);
   
    $img = 'https://rebelcitytour.com/wp-content/uploads/2023/03/duotone-logo.png';
            
    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->Host     = 'smtp.titan.email';
    $mail->Username = 'notification@rebelcitytour.com';
    $mail->Password = 'Rct@123#';
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = 'ssl';
    $mail->Port     = 465;
    $mail->SMTPOptions = array(
      'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
      )
    );
    //From email address and name
    $mail->setFrom('notification@rebelcitytour.com', 'Rebel City Tour');
    $mail->addAddress($customer_email);
    $mail->addCC('info@rebelcitytour.com');
    $mail->addCC('dev@hachiweb.com');
    $mail->isHTML(true);
    $mail->Subject = "Rebel City Tour Booking Confirmation-".$transaction_id;
    $mailContent = '<!DOCTYPE html>
    <html lang="en">
      <head>
        <title>Rebel City Tour Booking</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
      </head>
      <body style="border: 2px solid #e4e0e0; width: 98%;
      margin: auto; font-family: Times, Times New Roman, Georgia, serif">
        <table align="center" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td align="center">
                <table align="center" cellpadding="0" cellspacing="0" style="width:90%;">
                  <tbody>
                    <tr>
                      <td align="center">
                        <img style="width:6rem;" loading="lazy" src="'.$img.'">
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table align="center">
                  <hr style="color:black;">
                  <tbody>
                    <tr></tr>
                    <tr>
                      <h3 style="font-size: 1.6rem;">
                        Rebel City Tour Booking Confirmation<span style="color:#e8af83;"></span>
                      </h3>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <table align="center" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td align="center">
                <table align="center" cellpadding="0" cellspacing="0" style="width:90%;">
                  <tbody>
                    <tr>
                      <span align="center" style="font-size: 1.5rem;"><b>Name:</b> '.$customer_name.'
                      </span>
                    </tr>
                    <tr>
                      <span align="center" style="font-size: 1.5rem;"><b>Email:</b> '.$customer_email.'
                      </span>
                    </tr>
                    <tr>
                    <span align="center" style="font-size: 1.5rem;"><b>Phone: </b> '.$customer_phone.'
                    </span>
                  </tr>
                    <tr>
                      <span align="center" style="font-size: 1.5rem;"><b>Transaction id:</b> '.base64_encode($transaction_id).'
                      </span>
                    </tr>
                    <tr>
                      <span align="center" style="font-size: 1.5rem;"><b>Payment status:</b> Successful 
                      </span>
                    </tr>
                    <tr>
                      <span align="center" style="font-size: 1.5rem;"><b>Total amount:</b> '.$paid_amount.' '.$paid_currency.'
                      </span>
                    </tr>
                    <tr>
                      <span align="center" style="font-size: 1.5rem;"><b>Experience detail:</b> '.$experience_detail.'
                      </span>
                    </tr>
                    <tr>
                      <span align="center" style="font-size: 1.5rem;"><b>Duration:</b> '.str_replace(',','+',$experience_duration).' Minutes
                      </span>
                    </tr>
                    <tr>
                      <span align="center" style="font-size: 1.5rem;"><b>Booked for:</b> '.$ticket_qty.' Person
                      </span>
                    </tr>
                    <tr>
                      <span align="center" style="font-size: 1.5rem;"><b>Tour date & time:</b> '.$tour_date.' '.$tour_time.'
                      </span>
                    </tr>
                    <tr>
                      <span align="center" style="font-size: 1.5rem;"><b>Note:</b> '.$tour_note.'
                      </span>
                    </tr>
                    <tr>
                      <br>
                      <p align="center" style="font-size: 1.5rem;">
                        <b>
                          <a href="'.$ics_file_url.'" type="button" class="btn btn-lg btn-success">Export to my online calendar
                          </a>
                        </b> 
                      </p>
                    </tr> 
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <br>
        <table align="center" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td align="center">
                <table align="center" cellpadding="0" cellspacing="0" style="width:90%;">
                  <tbody>
                    <tr></tr>
                    <tr>
                      <h4 align="center" style="font-size: 1.5rem; font-weight:bold;">
                      THANKS,
                      </h4>
                      <h4 align="center" style="font-size: 1.5rem;">
                        REBEL CITY TOUR
                      </h4>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <table align="center" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td align="center">
                <table align="center" cellpadding="0" cellspacing="0" style="width:90%;">
                  <tbody>
                    <tr>
                      <p align="center" style="font-size: 1.2rem;">
                      Need help? Have feedback? Feel free to contact us at info@rebelcitytour.com, or at call us at +353 212411894</p>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <table align="center" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <p align="center" style="font-size: 1.2rem;">&copy; All Rights Reserved - <a href="https://rebelcitytour.com/">Rebel City Tour </a>'.date("Y").'
              </p>
            </tr>
          </tbody>
        </table>
      </body>
    </html>';
    $mail->Body = $mailContent;
    if ($mail->send()) 
    {
      $output['message'] = 'Congratulations! Your booking has been confirmed. Booking details have been sent on your email.';
    }
    else{
      $output['message'] = 'Congratulations! Your booking has been confirmed. Booking details have been sent on your email. '. $mail->ErrorInfo;
    }
    return $output;
  }

  function experience_data($experience_ids)
  {
    require_once dirname(__DIR__).'/templates/experiences/RCT_copy_US.php';
    $experience_ids = explode(',',$experience_ids);
    $experiences = (array)json_decode($experiences);
    $experience_data = array();
  
    foreach($experiences as $ekey1 => $eval1)
    {   
      $eval1_arr = (array)$eval1;
      $experience_id = $eval1_arr['id'];
    
      if(isset($eval1_arr['display']) && $eval1_arr['display'] == 1 && in_array($experience_id, $experience_ids))
      {
        $experience_title = $eval1_arr['title'];
        $experience_description = $eval1_arr['description'];
        $experience_duration = (int)$eval1_arr['duration']*80;
        $experience_cost = (float)((array)$eval1_arr['cost'])['eur'];
        $experience_data[$experience_id]['title'] = $experience_title;
        $experience_data[$experience_id]['description'] = $experience_description;
        $experience_data[$experience_id]['duration'] = $experience_duration;
        $experience_data[$experience_id]['cost'] = $experience_cost;
      }
    }
    return $experience_data;     
  }
}
?>
