<?php
require('TourBookingPayment.php');
/* Class to handle tour booking including create, read, update and delete */
class TourBooking extends TourBookingPayment
{ 
  function create_tour_booking_payment_intent_call($experience_ids,$ticket_qty)
  {
    return $this->create_tour_booking_payment_intent($experience_ids,$ticket_qty);
  }

  function create_tour_booking($tour_data)
  { 
    global $wpdb;
    $experience_data = $this->experience_data($tour_data['experience_id']);
    $all_titles = '';
    $all_descriptions = '';
    $all_durations = '';
    $total_price = 0;
    
    foreach($experience_data as $edk1 => $edv1)
    { 
      $total_price = $total_price+$edv1['cost'];
      if(empty($all_titles))
      {
        $all_titles = $edv1['title'];
        $all_descriptions = $edv1['description'];
        $all_durations = $edv1['duration'];
      }
      else
      {
        $all_titles = $all_titles.','.$edv1['title'];
        $all_descriptions = $all_descriptions.','.$edv1['description'];
        $all_durations = $all_durations.','.$edv1['duration'];
      }
    }
    $experience_ids = $tour_data['experience_id'];
    $customer_name = $tour_data['customer_name'];
    $customer_email = $tour_data['customer_email'];
    $customer_phone = $tour_data['customer_phone'];
    $tour_date = $tour_data['experience_date'];
    $tour_time = $tour_data['experience_time'];
    $appointment_reminder = $tour_data['experience_time'];
    $newsletter_subscription = $tour_data['newsletter_subscription'];
    $payment_intent_id = $tour_data['payment_intent_id'];
    $tour_note = $tour_data['tour_note'];
    $ticket_qty = (int)$tour_data['ticket_qty'];

    $table_name = $wpdb->prefix . "tour_bookings";
    $wpdb->insert($table_name, array(
      'experience_id' => $experience_ids,
      'experience_detail' => $all_titles,
      'experience_duration' => $all_durations,
      'experience_cost' => $ticket_qty*$total_price, 
      'customer_name' => $customer_name,
      'customer_email' => $customer_email,
      'customer_phone' => $customer_phone, 
      'tour_date' => $tour_date,
      'tour_time' => $tour_time,
      'appointment_reminder' => $appointment_reminder, 
      'newsletter_subscription' => $newsletter_subscription,
      'tour_note' => $tour_note,
      'ticket_qty' => $ticket_qty,
      'payment_status' => 0,
      'booking_status' => 1, 
      'stripe_customer_id' => '',
    ));
    $tour_data['booking_id'] = $wpdb->insert_id;
    return $this->create_tour_booking_customer($tour_data);
  }

  function update_tour_booking_payment($transaction_data)
  {
    return $this->tour_booking_payment($transaction_data);
  }
}